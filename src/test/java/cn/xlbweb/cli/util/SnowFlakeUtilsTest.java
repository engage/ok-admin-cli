package cn.xlbweb.cli.util;

import org.junit.Test;

/**
 * @author: bobi
 * @date: 2019-09-17 23:28
 * @description:
 */
public class SnowFlakeUtilsTest {

    @Test
    public void generatorId() {
        for (int i = 0; i < 100; i++) {
            long id = SnowFlakeUtils.getFlowIdInstance().nextId();
            System.out.println(String.valueOf(id));
        }
    }
}