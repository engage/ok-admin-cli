package cn.xlbweb.cli.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author: bobi
 * @date: 2018/11/7 22:31
 * @description:
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("cli")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.xlbweb.cli"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("bobi", "http://www.xlbweb.cn", "bobi1234@foxmail.com");
        return new ApiInfoBuilder()
                .title("RBAC权限开发脚手架Restful API文档")
                .description("关于作者")
                .termsOfServiceUrl("https://git.xlbweb.cn")
                .contact(contact)
                .version("v1.0")
                .build();
    }
}
